# Documentation contents {#doc-contents}

# General information

* [Introduction to Afivo](documentation/afivo_introduction.md)
* [Quadtree and octree grids](documentation/quadtree_octree.md)
* [Important data structures](documentation/data_structures.md)
* [Source code structure](documentation/source_code.md)
* [Terminology and variable names](documentation/terminology.md)

# How-to guides

* [Installation instructions](documentation/installation.md)
* [Defining the computational domain](documentation/computational_domain.md)
* [Modifying variables](documentation/modifying_variables.md)
* [Setting boundary conditions](documentation/boundary_conditions.md)
* [Filling ghost cells](documentation/filling_ghost_cells.md)
* [Performing grid refinement](documentation/grid_refinement.md)
* [Running in parallel](documentation/parallelization.md)
* [Working with multigrid](documentation/multigrid.md)
* [Writing and viewing output](documentation/writing_viewing_output.md)

# Examples

* A list of @ref examples-page

# Other

* [Related projects](documentation/other_projects.md)
* [FAQ](documentation/faq.md)
* [Authors](documentation/authors.md)
* [Bibliography](@ref citelist)
* [TODO list](@ref todo)
