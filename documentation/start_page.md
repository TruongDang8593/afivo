# Afivo

This is the documentation for **Afivo**, which is a framework for simulations on
adaptively refined [quadtree and octree grids](@ref quadtree_octree.md).

Have a look at the @ref doc-contents to get started with Afivo.

![Snapshot of a simulation performed with Afivo](branch_view.png)

**References / how to cite**

* \cite Nijdam_Teunissen_2016 Paper in which Afivo was first used
* \cite afivo_paper Paper describing Afivo
